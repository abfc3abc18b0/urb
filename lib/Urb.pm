package Urb;
use strict;
use warnings;

our $VERSION = '0.01';

=head1 NAME

Urb - Urban structures

=head1 SYNOPSIS

A framework for modelling and transforming buildings and other urban structures

=head1 DESCRIPTION

L<Urb::Quad> A fundamental unit of architectural space, four sided with
straight edges. Can be subdivided into two more L<Urb::Quad> objects, forming a
binary tree.

L<Urb::Dom> A subclass of L<Urb::Quad>, with additional methods and attributes
relevant to domestic architecture.

L<Urb::Dom::Fitness> A class for assessing the fitness of an L<Urb::Dom> object
in its environment.

L<Urb::Dom::Mutate> A class for applying a transformation/mutation to an
L<Urb::Dom> object.  Note that evolution is driven by crossover in addition to
mutation.

L<Urb::Boundary> Object representing the division of a quad and any child quads
that adjoin this division.

=head1 USAGE

  my $assessor = new Urb::Dom::Fitness;
  $rating = $assessor->_apply ($dom);

  my $mutator = new Urb::Dom::Mutate;
  $mutator->apply ($dom);

  $new_rating = $assessor->_apply ($dom);

  my $crossover = new Urb::Dom::Crossover;
  $crossover->apply ($dom->Left->Crossover, $dom_other->Right);

  $new_new_rating = $assessor->_apply ($dom);

=head1 AUTHOR

Bruno Postle <bruno@postle.net>

=head1 LICENSE AND COPYRIGHT

Copyright (c) 2004-2011 Bruno Postle <bruno@postle.net>.

This file is part of Urb.

Urb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Urb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Urb.  If not, see <http://www.gnu.org/licenses/>.

=cut

1;
