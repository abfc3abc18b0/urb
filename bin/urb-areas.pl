#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;
use 5.010;

say STDOUT join ', ', ('Path', 'Plot Area', 'Gross Area', 'Plot Ratio', 'Beds', 'Wcs');
for my $path_yaml (@ARGV)
{
    my $dom = Urb::Dom->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));

    my $area_plot = $dom->Area;
    my $area_internal = $dom->Area_Internal;
    my $count = $dom->Count;

    say STDOUT join ', ', ($path_yaml, $area_plot, $area_internal, $area_internal/$area_plot, scalar @{$count->{b}}, scalar @{$count->{t}});
}

0;
