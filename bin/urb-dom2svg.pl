#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom::Output;
use YAML;
use Carp;

for my $path_yaml (@ARGV)
{
    my $path_svg = $path_yaml.'.svg';

    my $dom = Urb::Dom::Output->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));

    my $svg = $dom->SVG;

    open my $SVGFILE, ">:encoding(UTF-8)", $path_svg or croak "$!";
    print $SVGFILE $svg->xmlify;
    close $SVGFILE;
}
