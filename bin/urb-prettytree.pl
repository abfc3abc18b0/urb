#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom::Output;
use YAML;

for my $path_yaml (@ARGV)
{
    my $quad = Urb::Dom::Output->new;
    $quad->Deserialise (YAML::LoadFile ($path_yaml));

    my $text = $quad->Pretty_Tree;
    print STDOUT $text;
}

0;
