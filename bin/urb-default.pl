#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom::Fitness;
use YAML;
use Carp;
use 5.010;

say "Urb::Dom::Fitness uses internal default values for pattern and cost
parameters.  These can be overridden by placing one or both of
patterns.config or costs.config in the main 'project' folder, or individually
in each of the 'plot' sub-folders.

This tool simply reads the defaults from Urb::Dom::Fitness and dumps the
settings to these files.

Note that you don't need to keep all the parameters in these files, just the
ones you want to override.
";

if (-e 'patterns.config') {
    say '...skipping existing patterns.config file, delete it first.';
}
else {
    say '...writing default configuration to patterns.config file.';
    YAML::DumpFile ('patterns.config', $Urb::Dom::Fitness::CONF);
}

if (-e 'costs.config') {
    say '...skipping existing costs.config file, delete it first.';
}
else {
    say '...writing default configuration to costs.config file.';
    YAML::DumpFile ('costs.config', $Urb::Dom::Fitness::COST);
}

exit 0;
