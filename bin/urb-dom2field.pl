#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use lib qw/lib/;
use Carp;

use YAML;
use File::Temp;
use File::Copy;
use File::Spec::Functions qw/splitpath/;
use Cwd qw/abs_path/;
use Urb::Dom;
use Urb::Field::Occlusion;

croak "usage: $0 dom_base.dom [dom_other1.dom dom_other2.dom some.walls ... ]" unless scalar @ARGV;

my $path_self = shift @ARGV;
my ($vol_self,$dir_self,$file_self) = splitpath(abs_path($path_self));

my $self = Urb::Dom->new;
$self->Deserialise (YAML::LoadFile ($path_self));

my $min_x = int ($self->Min->[0]);
my $max_x = int ($self->Max->[0]);
my $min_y = int ($self->Min->[1]);
my $max_y = int ($self->Max->[1]);
my $min_z = $self->Lowest->Elevation;
my $max_z = $self->Highest->Elevation + $self->Highest->Height;

my $walls = [];

for my $path (@ARGV)
{
    my ($vol,$dir,$file) = splitpath(abs_path($path));
    next if $dir eq $dir_self;
    next unless -e $path;
    if ($path =~ /\.dom$/xi)
    {
        my $dom = Urb::Dom->new;
        $dom->Deserialise (YAML::LoadFile ($path));
        push @{$walls}, $dom->Walls;
    }
    elsif ($path =~ /\.walls$/xi)
    {
        push @{$walls}, @{YAML::LoadFile ($path)};
    }
    say join ': ', $path, 'walls', scalar @{$walls};
}

my $occlusion = Urb::Field::Occlusion->new;
$occlusion->{origin} = [$min_x,$min_y,$min_z];
$occlusion->{_walls} = $walls;

my ($min_z_tmp, $max_z_tmp) = $occlusion->Walls_lowhigh;
$max_z = $max_z_tmp if (defined $max_z_tmp and $max_z_tmp > $max_z);
$max_z = int $max_z +1 if ($max_z > int $max_z);

say join ' ', 'Min:', $min_x, $min_y, $min_z;
say join ' ', 'Max:', $max_x, $max_y, $max_z;

for my $x ($min_x .. $max_x)
{
    for my $y ($min_y .. $max_y)
    {
        for my $z ($min_z .. $max_z)
        {
            for my $angle (0 .. 15)
            {
                $occlusion->Get_int ([$x,$y,$z], $angle);
            }
        }
    }
}

my ($fh, $path_tempfile) = File::Temp::tempfile();
YAML::DumpFile ($path_tempfile, $occlusion->Serialise);
copy ($path_tempfile, 'occlusion.field');

0;

__END__

=head1 NAME

urb-dom2field - Generate an occlusion field and write to a file

=head1 SYNOPSIS

urb-dom2field.pl <defines_area_of_interest.dom> <neighbour_1.dom> <neighbour_2.dom> [...]

=head1 DESCRIPTION

B<urb-dom2field> takes an L<Urb::Dom> object that defines a 'plot' where we are
interested in an occlusion field.  It also takes a list of other L<Urb::Dom>
objects containing structure defining walls etc...

For a grid of points in the area of interest, an ntuple is created describing
the horizon occlusion angle for a full 360 degrees.

This L<Urb::Field::Occlusion> object is serialised as a file named
'occlusion.field' and written to the current directory.

Tools such as L<urb-fitness.pl> must look for and read this 'occlusion.field'
before calculating a fitness.

=head1 COPYRIGHT

Copyright (c) 2004-2013 Bruno Postle <bruno@postle.net>.

This file is part of Urb.

Urb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Urb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Urb.  If not, see <http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb>

=cut

