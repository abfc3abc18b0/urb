#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use Urb::Dom::Mutate;
use Urb::Dom::Crossover;
use YAML;
use Carp;

my $DEBUG = $ENV{DEBUG};

my $path_yaml_a = shift || croak "Usage $0 some_dom_object.dom another_dom_object.dom";
my $path_yaml_b = shift || croak;

my $dom_a = Urb::Dom->new;
my $dom_b = Urb::Dom->new;
$dom_a->Deserialise (YAML::LoadFile ($path_yaml_a));
$dom_b->Deserialise (YAML::LoadFile ($path_yaml_b));

my $mutator = Urb::Dom::Mutate->new;
my $crossover = Urb::Dom::Crossover->new;

# 80% recombination 20% mutation
if (int rand (5))
{
    # exchange victims
    $crossover->apply ($dom_a, $dom_b);

    # do one mutation in A
    $dom_a = $mutator->apply ($dom_a);
}
else
{
    print "mutating\n" if $DEBUG;
    # do one mutation in A
    $dom_a = $mutator->apply ($dom_a);

    # do two mutations in B
    $dom_b = $mutator->apply ($dom_b);
    $dom_b = $mutator->apply ($dom_b);
}

my $hash_a = $dom_a->Hash;
my $hash_b = $dom_b->Hash;

YAML::DumpFile ($hash_a .'.dom', $dom_a->Serialise);
YAML::DumpFile ($hash_b .'.dom', $dom_b->Serialise);

exit 0;

__END__

=head1 NAME

urb-combine - Crossover two Urb::Dom files

=head1 SYNOPSIS

urb-combine <some_dom_object.dom> <another_dom_object.dom>

=head1 DESCRIPTION

B<urb-combine> takes two L<Urb::Dom> objects that have been serialised as
L<YAML> files, selects a sub-branch from each tree and exchanges them, this is
analogous to genetic crossover in living organisms.  The resulting offspring
are then written in L<YAML> format to disk using filenames derived from a hash
of the serialised content.

Note that currently a mutation is also applied using L<Urb::Dom::Mutate>.

=head1 COPYRIGHT

Copyright (c) 2004-2011 Bruno Postle <bruno@postle.net>.

This file is part of Urb.

Urb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Urb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Urb.  If not, see <http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb>

=cut

