#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;

for my $path_yaml (@ARGV)
{
    my $dom = Urb::Dom->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));

    $dom->Rotate;
    $dom->Straighten_Recursive;

    YAML::DumpFile ($path_yaml, $dom->Serialise);
}
