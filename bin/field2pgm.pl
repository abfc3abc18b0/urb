#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use YAML;
use lib 'lib';
use Urb::Field::Occlusion;
use Urb::Math qw /add_2d/;
use Carp;

our $PI = atan2 (0,-1);

my $path_yaml = shift;
open my $YAML, '<', $path_yaml or croak "$!";
my $yaml = join '', (<$YAML>);
close $YAML;

my $field = Urb::Field::Occlusion->new;
$field->Deserialise ($yaml);

my $size_x = scalar @{$field->{_data}};
my $size_y = scalar @{$field->{_data}->[0]};
my $size_z = scalar @{$field->{_data}->[0]->[0]};
my $directions = scalar @{$field->{_data}->[0]->[0]->[0]};

my $scale = 8;

use Urb::Misc::Sun;
my $sun = Urb::Misc::Sun->new (53.3814);
for my $z (0 .. $size_z -1)
{
    my $string;
    for my $y (reverse (0 .. ($size_y*$scale) -1))
    {
        for my $x (0 .. ($size_x*$scale) -1)
        {
            my $real = add_2d ([$x/$scale, $y/$scale,0], $field->{origin});
            my $value = int (255 / 4500 / 60 * $field->Sun_horizontal ($sun, [$real->[0],$real->[1],$z]));
            $string .= $value .' ';
        }
        $string .= "\n";
    }

    my $text_z = sprintf("%03d", $z);
    my $path_pgm = "field-sun-". $text_z. ".pgm";
    say $path_pgm;

    open my $PGM, '>', $path_pgm or croak "$!";
    say $PGM "P2";
    say $PGM "# Level $z";
    say $PGM $size_x * $scale .' '. $size_y * $scale;
    say $PGM 255;
    print $PGM $string;
    close $PGM;
}

for my $z (0 .. $size_z -1)
{
    my $string;
    for my $y (reverse (0 .. ($size_y*$scale) -1))
    {
        for my $x (0 .. ($size_x*$scale) -1)
        {
            my $real = add_2d ([$x/$scale, $y/$scale,0], $field->{origin});
            my $value = int (255 / 2.44346095279206 * $field->CIEsky_horizontal ([$real->[0],$real->[1],$z]));
            $string .= $value .' ';
        }
        $string .= "\n";
    }

    my $text_z = sprintf("%03d", $z);
    my $path_pgm = "field-". $text_z. ".pgm";
    say $path_pgm;

    open my $PGM, '>', $path_pgm or croak "$!";
    say $PGM "P2";
    say $PGM "# Level $z";
    say $PGM $size_x * $scale .' '. $size_y * $scale;
    say $PGM 255;
    print $PGM $string;
    close $PGM;
}

for my $z (0 .. $size_z -1)
{
    my $text_z = sprintf("%03d", $z);
    for my $direction (0 .. $directions -1)
    {
        my $string;
        for my $y (reverse (0 .. ($size_y*$scale) -1))
        {
            for my $x (0 .. ($size_x*$scale) -1)
            {
                my $real = add_2d ([$x/$scale, $y/$scale,0], $field->{origin});
#                my $value = $field->Get_interpolated_position ([$real->[0],$real->[1], $z], $direction);
             
#                $value = 255 - int (256 * 2 * $value / $PI);
                my $value = int (255 / 0.608335238775694 * $field->CIEsky_vertical ([$real->[0],$real->[1],$z], $PI * $direction / 8));
                $string .= $value .' ';
            }
            $string .= "\n";
        }

        my $path_pgm = "field-". $text_z. "-". sprintf("%02d",$direction). ".pgm";
        say $path_pgm;

        open my $PGM, '>', $path_pgm or croak "$!";
        say $PGM "P2";
        say $PGM "# Level $z, direction $direction";
        say $PGM $size_x * $scale .' '. $size_y * $scale;
        say $PGM 255;
        print $PGM $string;
        close $PGM;
    }
#    system ('convert', '-geometry', '100%', '-average', "field-$text_z-*.pgm", "field-$text_z.png");
    system ('convert', '-geometry', '100%', "field-$text_z-*.pgm", "field-$text_z.gif");
}

0;
