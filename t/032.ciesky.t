#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
use Urb::Misc::CIESky qw /average_vertical average_horizontal/;

use Math::Trig;

my $theta_90 = deg2rad (90.0);
my $theta_45 = deg2rad (45.0);
my $theta_00 = deg2rad (00.0);

ok (average_vertical ($theta_90) > average_vertical ($theta_45), 'greater');
ok (average_vertical ($theta_45) > average_vertical ($theta_00), 'greater');

ok (average_horizontal ($theta_90) > average_horizontal ($theta_45), 'greater');
ok (average_horizontal ($theta_45) > average_horizontal ($theta_00), 'greater');

ok (!average_vertical ($theta_00), 'no light when completely obstructed');
ok (!average_horizontal ($theta_00), 'no light when completely obstructed');

1;
