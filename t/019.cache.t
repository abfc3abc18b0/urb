#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Dom::Fitness;
use YAML;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/types.dom')), 'deserialise');

# +-----+---------+-----+-----+
# |lrll |lrlrr    |lrr  |rr   |
# |O    |O        |L    |O    |
# |     +---------+     |     |
# |     |lrlrl    |     +-----+
# |     |C        |     |rlr  |
# +-----+-+-----+-+-----+K    |
# |lllrlr |lllrr|llr    |     |
# |T      |C    |O      |     |
# +-------+     |       +-----+
# |lllrll |     |       |rll  |
# |B      |     |       |O    |
# +-----+-+-----+       |     |
# |lllll|llllr  |       |     |
# |O    |B      |       |     |
# +-----+-------+-------+-----+


my $assessor = Urb::Dom::Fitness->new;

$assessor->{_occlusion} = $dom->Occlusion (undef);
$assessor->{_occlusion}->{nocache} = 1;

my $score = sprintf ("%.20f", $assessor->_apply ($dom));
my @failures = $dom->Failures;

my $score2 = sprintf ("%.20f", $assessor->_apply ($dom));
my @failures2 = $dom->Failures;

is ($score, $score2, 'calculation using cache is same as original');
is_deeply ([@failures], [@failures2], 'calculation using cache is same as original');

my $old_div = $dom->By_Id ('')->{division};

$dom->By_Id ('')->{division} = [0.3,0.3];

my $score3 = sprintf ("%.20f", $assessor->_apply ($dom));
for (0 .. 20) {$assessor->_apply ($dom)};

ok ($score != $score3, 'altered dom has different score');

$dom->By_Id ('')->{division} = $old_div;

is ($score, $score2, 'calculation using cache and resored dom is same as original');

1;
