#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests Urb::Boundary length bug

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/tooshort.dom'));

my $boundaries = $quad->Calc_Boundaries;

my $boundary = $boundaries->{'lr'};
ok ($boundary->Validate_Ids, 'boundary validates');

is (scalar @{$boundary}, 5, 'five quads on boundary lr');

ok ($boundary->Length_Total =~ /^5\.9341288/x, 'length');

TODO:
{
local $TODO = "4-way corner not counted";
is (scalar @{$boundary->Pairs}, 5, 'five overlaps');
}

1;
