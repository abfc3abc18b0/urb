#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
use_ok ('Urb::Dom');

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

is ($dom->L->L->Usage, undef, 'Usage()');

is ($dom->L->L->L->Usage, 'Living', 'Usage()');
is ($dom->L->L->R->Usage, 'Circulation', 'Usage()');
is ($dom->L->R->R->L->Usage, 'Kitchen', 'Usage()');
is ($dom->L->R->R->R->Usage, 'Outside', 'Usage()');
is ($dom->Above->L->R->R->Usage, 'Bedroom', 'Usage()');
is ($dom->Above->L->R->L->R->Usage, 'Toilet', 'Usage()');

is ($dom->R->L->Usage, 'Outside', 'Usage()');
is ($dom->Above->R->L->Usage, undef, 'Usage()');

is ($dom->L->L->L->Usage, 'Living', 'Usage()');
$dom->L->L->L->Type ('Attic');
is ($dom->L->L->L->Usage, undef, 'Usage()');
