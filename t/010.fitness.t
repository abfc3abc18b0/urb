#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Dom::Fitness;
use YAML;

my $dom = Urb::Dom->new;
my $fitness = Urb::Dom::Fitness->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/types.dom')), 'deserialise');

# +-----+---------+-----+-----+
# |lrll |lrlrr    |lrr  |rr   |
# |O    |O        |L    |O    |
# |     +---------+     |     |
# |     |lrlrl    |     +-----+
# |     |C        |     |rlr  |
# +-----+-+-----+-+-----+K    |
# |lllrlr |lllrr|llr    |     |
# |T      |C    |O      |     |
# +-------+     |       +-----+
# |lllrll |     |       |rll  |
# |B      |     |       |O    |
# +-----+-+-----+       |     |
# |lllll|llllr  |       |     |
# |O    |B      |       |     |
# +-----+-------+-------+-----+


is ($fitness->cost_leaf ($dom->R->L->R), 4480, 'K cost_leaf');
is ($fitness->cost_leaf ($dom->R->R), 192, 'O cost_leaf');

ok (!$dom->R->R->Is_Covered, 'Is_Covered()');
ok (!$dom->R->R->Is_Supported, 'Is_Supported()');

is ($fitness->cost_edge_outside ($dom->R->L->R), 2240, 'rlr cost_edge_outside');
is ($fitness->cost_edge_outside ($dom->R->R), 440, 'rr cost_edge_outside');
is ($fitness->cost_edge_outside ($dom->L->R->L->R->L), 0, 'lrlrl cost_edge_outside');
is ($fitness->cost_edge_outside ($dom->L->L->L->R->R), 0, 'lllrr cost_edge_outside');

my $graph = $dom->Graph;

is ($fitness->cost_edge ($graph, [$dom->L->R->R, $dom->R->L->R]), 320, 'cost_edge inside');
is ($fitness->cost_edge ($graph, [$dom->L->L->R, $dom->R->L->L]), 0, 'cost_edge outside');
is ($fitness->cost_edge ($graph, [$dom->L->R->R, $dom->L->L->R]), 1260, 'cost_edge both');

