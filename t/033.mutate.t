#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
BEGIN { use_ok('Urb::Dom::Mutate') };
use Urb::Dom;
use YAML;

my $mutator = Urb::Dom::Mutate->new;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

ok (my $dom2 = $mutator->apply ($dom), 'apply()');
ok (my $dom3 = $mutator->apply_rearrange ($dom), 'apply_rearrange');

ok (my $dom4 = $mutator->apply ($dom->By_Level (1)), 'apply()');
ok (my $dom5 = $mutator->apply ($dom->L->L->L), 'apply()');
ok (my $dom6 = $mutator->apply ($dom), 'apply()');
ok (my $dom7 = $mutator->apply ($dom), 'apply()');
ok (my $dom8 = $mutator->apply ($dom), 'apply()');

#branchnodes

ok (Urb::Dom::Mutate::rotate ($dom->By_Level (1)->L->L), 'rotate');
ok (Urb::Dom::Mutate::slide ($dom->By_Level (1)->L->L), 'slide');
ok (Urb::Dom::Mutate::slide ($dom->By_Level (1)->L->L), 'slide');
ok (Urb::Dom::Mutate::slide ($dom->By_Level (1)->L->L), 'slide');
ok (Urb::Dom::Mutate::slide ($dom->By_Level (1)->L->L), 'slide');
ok (Urb::Dom::Mutate::height ($dom->By_Level (1)->L->L), 'height');

$dom->By_Level (1)->{height} = 4;
ok (Urb::Dom::Mutate::height ($dom->By_Level (1)->L->L), 'height');
is ($dom->By_Level (1)->{height}, 3.6, 'By_Level()');
ok (Urb::Dom::Mutate::height ($dom->By_Level (1)->L->L), 'height');
ok (Urb::Dom::Mutate::height ($dom->By_Level (1)->L->L), 'height');
$dom->By_Level (1)->{height} = 1;
ok (Urb::Dom::Mutate::height ($dom->By_Level (1)->L->L), 'height');
is ($dom->By_Level (1)->{height}, 2.7, 'By_Level()');

ok (Urb::Dom::Mutate::swap ($dom->By_Level (1)->L->L), 'swap');
ok (Urb::Dom::Mutate::undivide ($dom->By_Level (1)->L->L), 'undivide');

ok (Urb::Dom::Mutate::level_swap ($dom->By_Level (1)->L->L), 'level_swap success for 1');
ok (Urb::Dom::Mutate::level_swap ($dom->By_Level (0)->L->L), 'level_swap success for 0');

ok (Urb::Dom::Mutate::level_add ($dom->By_Level (1)), 'level_add');

ok (Urb::Dom::Mutate::level_delete ($dom->By_Level (2)), 'level_delete');
ok (Urb::Dom::Mutate::level_delete ($dom->By_Level (1)), 'level_delete');
ok (!Urb::Dom::Mutate::level_delete ($dom->By_Level (0)), 'level_delete fails for 0');

#leafnodes

ok (Urb::Dom::Mutate::type ($dom->By_Level (0)->R->R->R), 'type');
ok (Urb::Dom::Mutate::type ($dom->By_Level (0)->R->R->R), 'type');

my $type = $dom->By_Level (0)->R->R->R->Type;

ok (Urb::Dom::Mutate::divide ($dom->By_Level (0)->R->R->R), 'divide');

ok ($dom->By_Level (0)->R->R->R->L->Type eq $type || $dom->By_Level (0)->R->R->R->R->Type eq $type, 'child');

ok (!Urb::Dom::Mutate::debug(), 'debug()');
$Urb::Dom::Mutate::DEBUG = 1;
ok (Urb::Dom::Mutate::debug(), 'debug()');

1;
