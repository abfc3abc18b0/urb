#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Dom') };
use Urb::Misc::Stairs qw/ideal_going risers_number/;

# Tests for methods that return lists of Urb::Quad objetcs

my $dom = Urb::Dom->new;

$dom->{node} = [[0.0,0.0],[2.95,0.0],[2.95,2.725],[0.0,2.725]];

#Given a max riser height, the stair width, and a list of corners that must be clear, can we fit the stair?

my $risers = risers_number ($dom->Height, 0.21);
my $going = ideal_going ($dom->Height / $risers);

is ($risers, 15, 'risers');
is ($going, 0.225, 'going');

is ($dom->Stair_Fit (0.21, 1.25, 2), 1, 'Stair_Fit()');
is ($dom->Stair_Fit (0.21, 1.25, 1), 1, 'Stair_Fit()');

ok ($dom->Stair_Fit (0.21, 1.25, 2, 3) < 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1, 2) < 1, 'Stair_Fit()');

ok ($dom->Stair_Fit (0.21, 1.25, 2, 3, 4) < 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1, 2, 3) < 1, 'Stair_Fit()');

ok ($dom->Stair_Fit (0.21, 1.25, 2, 3, 4, 5) < 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1, 2, 3, 4) < 1, 'Stair_Fit()');

$dom->{node} = [[0.0,0.0],[2.95,0.0],[2.95,3.175],[0.0,3.175]];

ok ($dom->Stair_Fit (0.21, 1.25, 2) > 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1) > 1, 'Stair_Fit()');

is ($dom->Stair_Fit (0.21, 1.25, 2, 3), 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1, 2) < 1, 'Stair_Fit()');

ok ($dom->Stair_Fit (0.21, 1.25, 2, 3, 4) < 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1, 2, 3) < 1, 'Stair_Fit()');

ok ($dom->Stair_Fit (0.21, 1.25, 2, 3, 4, 5) < 1, 'Stair_Fit()');
ok ($dom->Stair_Fit (0.21, 1.25, 1, 2, 3, 4) < 1, 'Stair_Fit()');

1;
