#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use YAML;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/types.dom')), 'deserialise');

# +-----+---------+-----+-----+
# |lrll |lrlrr    |lrr  |rr   |
# |O    |O        |L    |O    |
# |     +---------+     |     |
# |     |lrlrl    |     +-----+
# |     |C        |     |rlr  |
# +-----+-+-----+-+-----+K    |
# |lllrlr |lllrr|llr    |     |
# |T      |C    |O      |     |
# +-------+     |       +-----+
# |lllrll |     |       |rll  |
# |B      |     |       |O    |
# +-----+-+-----+       |     |
# |lllll|llllr  |       |     |
# |O    |B      |       |     |
# +-----+-------+-------+-----+

my $list = $dom->By_Area (10.0);

#for my $item (@{$list})
#{
#    print join ' ', $item->[1]->Id, $item->[1]->Area, $item->[0], "\n";
#}

ok ($list->[0]->[1]->Id eq 'lllrlr', 'Id()');
ok ($list->[1]->[1]->Id eq 'lllrr', 'Id()');
ok ($list->[2]->[1]->Id eq 'lrlrl', 'Id()');
ok ($list->[3]->[1]->Id eq 'lllll', 'Id()');
ok ($list->[4]->[1]->Id eq 'lllrll', 'Id()');

