#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
use Urb::Misc::Sun;

is (Urb::Misc::Sun::latitudes(), 64, 'latitudes()');
is (Urb::Misc::Sun::azimuths(), 16, 'azimuths()');
is (Urb::Misc::Sun::elevations(), 32, 'elevations()');

# use Data::Dumper; die Dumper Urb::Misc::Sun::table (90.0);

my $table = Urb::Misc::Sun::table (-90.0);
is ($table->[15]->[8], 2796, 'lookup');

$table = Urb::Misc::Sun::table (-89.9);
ok ($table->[15]->[8] > 2796, 'lookup');

$table = Urb::Misc::Sun::table (0.0);
is ($table->[15]->[8], 4785, 'lookup');
is ($table->[15]->[9], 4785, 'lookup');

$table = Urb::Misc::Sun::table (90.0);
is ($table->[15]->[8], 2796, 'lookup');

my $sun = Urb::Misc::Sun->new (-90.0);
is ($sun->Minutes (6.0868, 0.000), 16116, 'minutes');
ok ($sun->Minutes (6.0868, 0.049087385) =~ /^14766\./x, 'minutes');

ok ($sun->Minutes (6.0868, 0.024543692) =~ /^15441\./x, 'minutes');
ok ($sun->Minutes (6.0868, 1.52170894) == 0, 'minutes');
ok ($sun->Minutes (6.0868, 1.57079632) == 0, 'minutes');
is ($sun->Minutes (6.0868, 1.57079633), undef, 'minutes');

ok ($sun->Minutes (6.0800, 0.39269908) =~ /^2796\./x, 'minutes');

ok ($sun->Minutes (6.0968, 0.39269908) =~ /^2791\./x, 'minutes');
ok ($sun->Minutes (0.0, 0.39269908) =~ /^2791\./x, 'minutes');

my $sun2 = $sun->new (90);

1;
