#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
BEGIN { use_ok('Urb::Field') };

my $field = Urb::Field->new;

$field->{_data} = [ [[2,3],[9,5]],[[4,1],[6,7]] ];

is ($field->Get_int ([0,0,0]), 2, 'Get_int()');
is ($field->Get_int ([1,0,0]), 4, 'Get_int()');
is ($field->Get_int ([0,1,0]), 9, 'Get_int()');
is ($field->Get_int ([1,1,0]), 6, 'Get_int()');
is ($field->Get_int ([0,0,1]), 3, 'Get_int()');
is ($field->Get_int ([0,1,1]), 5, 'Get_int()');
is ($field->Get_int ([1,0,1]), 1, 'Get_int()');
is ($field->Get_int ([1,1,1]), 7, 'Get_int()');

is ($field->Get_int ([-1,0,0]), undef, 'Get_int <0');
is ($field->Get_int ([2,0,0]), undef, 'Get_int >1');

is (Urb::Field::_max_xyz ([0,0,0], [5,0,0]), 5, '_max_xyz');
is (Urb::Field::_max_xyz ([1,1,1], [0,0,0]), 1, '_max_xyz');
is (Urb::Field::_max_xyz ([5,3,4], [5,0,0]), 4, '_max_xyz');
is (Urb::Field::_max_xyz ([5,3,4], [15,-3,0]), 10, '_max_xyz');

my @results = $field->Get_weighted ([0.2,0.9,0.5]);
is (scalar (@results), 8, 'eight results');
my $sum = undef;
for my $result (@results)
{
    $sum += $result->[1];
}
is ($sum, 1, 'weights add to 1.0');

@results = $field->Get_weighted ([0.2,0.9,1.5]);
is (scalar (@results), 4, 'four results');
$sum = undef;
for my $result (@results)
{
    $sum += $result->[1];
}
is ($sum, 1, 'weights add to 1.0');

@results = $field->Get_weighted ([0.2,1.9,1.5]);
is (scalar (@results), 2, 'two results');
$sum = undef;
for my $result (@results)
{
    $sum += $result->[1];
}
is ($sum, 1, 'weights add to 1.0');

@results = $field->Get_weighted ([1.2,1.9,1.5]);
is (scalar (@results), 1, 'one results');
$sum = undef;
for my $result (@results)
{
    $sum += $result->[1];
}
is ($sum, 1, 'weights add to 1.0');

$field->{origin} = [9,0,0];

@results = $field->Get_weighted ([0.2,0.9,0.5]);
is (scalar (@results), 0, 'zero results when origin moved');
$sum = undef;
for my $result (@results)
{
    $sum += $result->[1];
}
is ($sum, undef, 'weights undef');

$field->{origin} = [0,0,0];

@results = $field->Get_weighted ([0.2,0.9,0.5]);
is (scalar (@results), 8, 'eight results');
$sum = undef;
for my $result (@results)
{
    $sum += $result->[1];
}
is ($sum, 1, 'weights add to 1.0');

1;
