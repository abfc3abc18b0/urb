#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use YAML;

my $yaml = "---
elevation: 2.0
id: ''
node:
  -
    - 17
    - 49
  -
    - 23
    - 35
  -
    - 35
    - 39
  -
    - 27
    - 53
perimeter:
  a: private
  b: private
  c: ~
  d: private
wall_inner: 0.1
wall_outer: 0.5
rotation: 0
type: O\n";

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::Load ($yaml)), 'deserialise');

like ($dom->Coordinate(0)->[0], '/^17.66/', 'node coordinates are inset by wall_outer');

my @yaml_out = $dom->Serialise;
is ($yaml_out[0]->{node}->[0]->[0], 17, 'Deserialise eq Serialise');

$dom->Rotate;
@yaml_out = $dom->Serialise;
is ($yaml_out[0]->{node}->[0]->[0], 17, 'Rotate(), Deserialise eq Serialise');
is ($yaml_out[0]->{rotation}, 1, 'Rotate()');

$dom = Urb::Dom->new;
$dom->Deserialise (@yaml_out);
@yaml_out = $dom->Serialise;
is ($yaml_out[0]->{node}->[0]->[0], 17, 'Rotate(), Deserialise eq Serialise');
is ($yaml_out[0]->{rotation}, 1, 'Rotate()');


