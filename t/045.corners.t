#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that return lists of Urb::Quad objetcs

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#     quad Ids
#  +---------+------+
#  |   rrl   |      |
#  +---------+  rl  |
#  |   rrr   |      |
#  +------+--+------+
#  |  lrr |    lrl  |
#  +------+---------+
#  |        ll      |
#  +----------------+

my $graph = $quad->Graph;

my @ids = $quad->L->L->Corners_In_Use ($graph, $quad->L->R->R);
is (scalar (@ids), 1, 'Corners_In_Use()');

@ids = $quad->L->L->Corners_In_Use ($graph, $quad->L->R->R, $quad->L->R->L);
is (scalar (@ids), 2, 'Corners_In_Use()');

@ids = $quad->L->R->L->Corners_In_Use ($graph, $quad->L->R->R);
is (scalar (@ids), 1, 'Corners_In_Use()');

@ids = $quad->L->R->L->Corners_In_Use ($graph, $quad->L->R->R, $quad->R->R->R);
is (scalar (@ids), 1, 'Corners_In_Use()');

@ids = $quad->L->R->L->Corners_In_Use ($graph, $quad->L->R->R, $quad->L->L);
is (scalar (@ids), 1, 'Corners_In_Use()');

@ids = $quad->L->R->L->Corners_In_Use ($graph, $quad->L->R->R, $quad->R->L);
is (scalar (@ids), 2, 'Corners_In_Use()');

@ids = $quad->L->R->L->Corners_In_Use ($graph, $quad->L->R->R, $quad->R->L, $quad->R->R->R);
is (scalar (@ids), 2, 'Corners_In_Use()');

@ids = $quad->L->R->L->Corners_In_Use ($graph, $quad->L->R->R, $quad->R->L, $quad->L->L);
is (scalar (@ids), 3, 'Corners_In_Use()');

@ids = $quad->R->R->R->Corners_In_Use ($graph, $quad->R->L);
is (scalar (@ids), 1, 'Corners_In_Use()');

@ids = $quad->R->R->R->Corners_In_Use ($graph, $quad->R->L, $quad->R->R->L);
is (scalar (@ids), 1, 'Corners_In_Use()');

@ids = $quad->R->R->R->Corners_In_Use ($graph, $quad->R->L, $quad->R->R->L, $quad->L->R->R);
is (scalar (@ids), 3, 'Corners_In_Use()');

1;
