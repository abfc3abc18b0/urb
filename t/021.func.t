#!/usr/bin/perl
#Editor vim:syn=perl

use strict;
use warnings;

use Test::More 'no_plan';
use lib ('lib', '../lib');

use Urb::Math qw /distance_2d scale_2d add_2d normalise_2d is_between_2d angle_2d subtract_2d points_2line perpendicular_line line_intersection perpendicular_distance is_angle_between/;

is (distance_2d ([0,0], [3,4]), 5, 'distance');

my $a = [1,2];
my $b = scale_2d ($a, 2);

is_deeply ($b, [2,4], 'scale vector');

my $c = add_2d ($a, $b);

is_deeply ($c, [3,6], 'add vectors');

my $d = add_2d ($a, $b, $c);

is_deeply ($d, [6,12], 'add vectors');

is_deeply (normalise_2d ([3,4]), [0.6,0.8], 'normalise_2d');
is_deeply (normalise_2d ([-3,-4]), [-0.6,-0.8], 'normalise_2d');
is_deeply (normalise_2d ([0,1]), [0,1], 'normalise_2d');
is_deeply (normalise_2d ([0,0]), [1,0], 'normalise_2d');

ok (is_between_2d ([1,0],[0,0],[2,0]), 'is_between_2d');
ok (!is_between_2d ([0,0],[1,0],[2,0]), 'is_between_2d');
ok (is_between_2d ([5,5],[-1,-1],[9,9]), 'is_between_2d');
ok (is_between_2d ([5,15],[-1,9],[9,19]), 'is_between_2d');
ok (is_between_2d ([-5,15],[-11,9],[-1,19]), 'is_between_2d');
ok (!is_between_2d ([-5,15.1],[-11,9],[-1,19]), 'is_between_2d');
ok (!is_between_2d ([-5,15.01],[-11,9],[-1,19]), 'is_between_2d');
ok (is_between_2d ([-5,15.001],[-11,9],[-1,19]), 'is_between_2d');
ok (is_between_2d ([-5,15.0001],[-11,9],[-1,19]), 'is_between_2d');

is (distance_2d ([0,0],[3,4]), 5, 'distance_2d');
is (distance_2d ([-5,1],[-2,5]), 5, 'distance_2d');
is (distance_2d ([0,0],[0,0]), 0, 'distance_2d');
is (distance_2d ([10,20],[10,20]), 0, 'distance_2d');

is (distance_2d (5,[10,20]), 0, 'distance_2d');
is (distance_2d ([10,20],5), 0, 'distance_2d');

is (angle_2d ([2,2],[3,2]), 0, 'angle_2d');
like (angle_2d ([2,2],[3,3]), '/^0\.785/', 'angle_2d');
like (angle_2d ([2,2],[2,3]), '/^1\.570/', 'angle_2d');
like (angle_2d ([2,2],[1,3]), '/^2\.356/', 'angle_2d');
like (angle_2d ([2,2],[1,2]), '/^3\.141/', 'angle_2d');
like (angle_2d ([2,2],[1,1]), '/^-2\.356/', 'angle_2d');
like (angle_2d ([2,2],[2,1]), '/^-1\.570/', 'angle_2d');
like (angle_2d ([2,2],[3,1]), '/^-0\.785/', 'angle_2d');

is_deeply (scale_2d ([2,2], 1), [2,2], 'scale_2d');
is_deeply (scale_2d (1, [2,2]), [2,2], 'scale_2d');

is_deeply (subtract_2d ([3,2],[1,1]), [2,1], 'subtract_2d');
is_deeply (subtract_2d ([3,2],[5,1]), [-2,1], 'subtract_2d');

is_deeply (add_2d ([1,2],[5,-1]), [6,1], 'add_2d');
is_deeply (add_2d ([1,2],[5,-1],[7,2]), [13,3], 'add_2d');

is_deeply (points_2line ([1,1],[5,5]), {a=>1,b=>0}, 'points_2line');
is_deeply (points_2line ([1,1],[-5,-5]), {a=>1,b=>0}, 'points_2line');

is_deeply (perpendicular_line ({a=>0.5,b=>1}, [1,4]), {a=>-2,b=>6}, 'perpendicular_line');

ok (perpendicular_line ({a=>0.0,b=>1}, [1,4]), 'perpendicular_line');

is_deeply (line_intersection ({a=>0.5,b=>1},{a=>-2,b=>6}), [2,2], 'line_intersection');

is (line_intersection ({a=>0.5,b=>1},{a=>0.5,b=>6}), undef, 'line_intersection');

is (perpendicular_distance ({a=>0.75, b=>1}, [7,0]), 5, 'perpendicular_distance');

ok (is_angle_between (0.1,0.0,0.2), 'angle between');
ok (is_angle_between (0.1,0.2,0.1), 'angle between');
ok (is_angle_between (0.1,-0.1,0.2), 'angle between');
ok (is_angle_between (0.1,0.2,-0.2), 'angle between');
ok (is_angle_between (0.1,0.0,3.1), 'angle between');
ok (is_angle_between (0.1,0.0,3.1), 'angle between');
ok (is_angle_between (1.6,0.0,3.1), 'angle between');
ok (is_angle_between (1.6,3.1,0.1), 'angle between');
ok (is_angle_between (3.1,2.0,-2.0), 'angle between');
ok (is_angle_between (3.2,2.0,-2.0), 'angle between');
ok (is_angle_between (-3.1,2.0,-2.0), 'angle between');
ok (is_angle_between (-0.1,6.0,1.0), 'angle between');
ok (is_angle_between (-0.1,1.0,6.0), 'angle between');
ok (is_angle_between (6.1,1.0,6.0), 'angle between');
ok (is_angle_between (4.0,3.2,6.0), 'angle between');
ok (is_angle_between (-3.0,3.2,6.0), 'angle between');
ok (is_angle_between (4.0,3.2,-1.0), 'angle between');
ok (is_angle_between (-0.7,-0.2,-1.0), 'angle between');
ok (is_angle_between (5.58,-0.2,-1.0), 'angle between');

ok (!is_angle_between (2.0,1.0,6.0), 'angle between');
ok (!is_angle_between (5.9,-0.9,-1.0), 'angle between');
ok (!is_angle_between (-0.38,-0.9,-1.0), 'angle between');

