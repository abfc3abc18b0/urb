#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;
use Urb::Math qw /distance_2d triangle_area/;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that query Urb::Quad attributes

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

is ($quad->Right->Right->Right->Boundary_Id (0), 'd', 'boundary id');
is ($quad->Right->Right->Right->Boundary_Id (1), '', 'boundary id');
is ($quad->Right->Right->Right->Boundary_Id (2), 'r', 'boundary id');
is ($quad->Right->Right->Right->Boundary_Id (3), 'rr', 'boundary id');

is ($quad->Left->Left->Boundary_Id (0), 'b', 'boundary id');
is ($quad->Left->Left->Boundary_Id (1), 'l', 'boundary id');
is ($quad->Left->Left->Boundary_Id (2), 'd', 'boundary id');
is ($quad->Left->Left->Boundary_Id (3), 'a', 'boundary id');

is (distance_2d ([1,1], [4,5]), 5, 'pythagoras');
is (distance_2d ([-2,-6], [2,-3]), 5, 'pythagoras');

is (triangle_area ([0,0],[10,0],[0,10]), 50, 'triangle area');
is (triangle_area ([10,0],[-10,0],[0,0]), 0, 'triangle area');
is (triangle_area ([-10,-10],[-10,-5],[-5,-10]), 12.5, 'triangle area');

is ($quad->Length (0), 4, 'length');
is ($quad->Length (1)**2, 8, 'length');

my @id_edge = $quad->By_Length;
ok ($quad->Length ($id_edge[0]) <= $quad->Length ($id_edge[1]), 'edges sorted by length');
ok ($quad->Length ($id_edge[1]) <= $quad->Length ($id_edge[2]), 'edges sorted by length');
ok ($quad->Length ($id_edge[2]) <= $quad->Length ($id_edge[3]), 'edges sorted by length');

like ($quad->Angle (0) + $quad->Angle (1) + $quad->Angle (2) + $quad->Angle (3), '/^6.283185/', 'corner angles add up to 2*pi');

ok ($quad->Left->Left->Aspect > 1, 'aspect');
ok ($quad->Left->Right->Aspect > 1, 'aspect');
ok ($quad->Right->Left->Aspect > 1, 'aspect');
ok ($quad->Right->Right->Aspect > 1, 'aspect');

is ($quad->Left->Left->{rotation}, 0, 'rotation');
is ($quad->Left->Right->{rotation}, 1, 'rotation');
is ($quad->Right->Left->{rotation}, 0, 'rotation');
is ($quad->Left->Right->{rotation}, 1, 'rotation');

is ($quad->Left->Left->Rotation, 0, 'Rotation');
is ($quad->Left->Right->Rotation, 1, 'Rotation');
is ($quad->Right->Left->Rotation, 0, 'Rotation');
is ($quad->Left->Right->Rotation, 1, 'Rotation');

is ($quad->Left->Right->Rotation (2), 2, 'Rotation');
is ($quad->Left->Right->Rotation, 2, 'Rotation');
is ($quad->Left->Right->Rotation (1), 1, 'Rotation');

is (($quad->Left->Area + $quad->Right->Area) / $quad->Area, 1, 'areas add up');
is (($quad->Left->Left->Area + $quad->Left->Right->Area) /
     $quad->Left->Area, 1, 'areas add up');

is ($quad->By_Id ('')->Area, 8, 'area of root quad is 8');

1;
