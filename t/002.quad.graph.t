#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for the undirected graph version of the quad object

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

my $graph = $quad->Graph;
ok ($graph->is_simple_graph, 'graph has no multiedges');
ok ($graph->is_cyclic, 'graph has loops');
ok ($graph->is_undirected, 'graph is undirected');
ok ($graph->is_connected, 'graph has one connected group');
ok ($graph->is_edge_connected, 'no bridges here');
is (scalar $graph->vertices, 6, 'six vertices');
is (scalar $graph->edges, 9, 'nine links');
is (scalar $graph->cut_vertices, 0, 'no articulation');

my $clone = $graph->clone;

is_deeply ($graph, $clone, 'clone is same as graph');

1;
