#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
BEGIN { use_ok('Urb::Dom::Output') };

my $dom = Urb::Dom::Output->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

ok (my $dot1 = $dom->Dot_Tree, 'Dot_Tree');
like ($dot1, '/^digraph /', 'dot');

ok (my $dot2 = $dom->Dot_Net, 'Dot_Net');
like ($dot2, '/^(strict )?graph /', 'dot');

ok (my $svg = $dom->SVG, 'SVG');
like ($svg->xmlify, '/<svg /', 'xml');

ok (my $dxf = $dom->DXF, 'DXF');
like ($dxf, '/0\nSECTION\n *2\nENTITIES\n/', 'dxf');
like ($dxf, '/10\n *10.9995718006447\n/', 'dxf');

ok (my $tree = $dom->Pretty_Tree, 'Pretty_Tree');

