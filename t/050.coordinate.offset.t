#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };
use Urb::Math qw /distance_2d/;

my $quad = Urb::Quad->new;

$quad->{node} = [[-2,-5],[17,3],[16,9],[5,7]];

ok (distance_2d ($quad->Coordinate_Offset (0, 2.5), [-7.9341,-10.2111]) < 0.001, 'outset');
ok (distance_2d ($quad->Coordinate_Offset (1, 2.5), [19.7907,1.4625]) < 0.001, 'outset');
ok (distance_2d ($quad->Coordinate_Offset (2, 2.5), [18.0489,11.9135]) < 0.001, 'outset');
ok (distance_2d ($quad->Coordinate_Offset (3, 2.5), [3.4205,9.2538]) < 0.001, 'outset');

ok (distance_2d ($quad->Coordinate_Offset (0, -2.5), [3.9341,0.2111]) < 0.001, 'inset');
ok (distance_2d ($quad->Coordinate_Offset (1, -2.5), [14.2093,4.5375]) < 0.001, 'inset');
ok (distance_2d ($quad->Coordinate_Offset (2, -2.5), [13.9511,6.0865]) < 0.001, 'inset');
ok (distance_2d ($quad->Coordinate_Offset (3, -2.5), [6.5795,4.7462]) < 0.001, 'inset');

ok (distance_2d ($quad->Coordinate_Offset (0, 0.0), [-2,-5]) < 0.001, 'zero offset');
ok (distance_2d ($quad->Coordinate_Offset (0, 0), [-2,-5]) < 0.001, 'zero offset');
ok (distance_2d ($quad->Coordinate_Offset (0), [-2,-5]) < 0.001, 'zero offset');

use Urb::Dom;

my $dom = Urb::Dom->new;

$dom->{node} = [[-2,-5],[17,3],[16,9],[5,7]];

is ($dom->Wall_Inner, 0.08, 'Wall_Inner()');
$dom->{wall_inner} = 0.1;
is ($dom->Wall_Inner, 0.1, 'Wall_Inner()');

is ($dom->Wall_Outer, 0.25, 'Wall_Outer()');
$dom->{wall_outer} = 0.4;
is ($dom->Wall_Outer, 0.4, 'Wall_Outer()');

is ($dom->Stair_Riser, 0.21, 'Stair_Riser()');
$dom->{stair_riser} = 0.150;
is ($dom->Stair_Riser, 0.15, 'Stair_Riser()');

is ($dom->Stair_Width, 1.25, 'Stair_Width()');
$dom->{stair_width} = 1.000;
is ($dom->Stair_Width, 1, 'Stair_Width()');

my @list = $dom->Serialise;

is ($list[0]->{wall_inner}, 0.1, 'Serialise()');

$dom->Divide;
$dom->L->Type ('C');
$dom->R->Type ('C');

ok ($dom->Divided, 'Merge_Divided()');
$dom->Merge_Divided;
ok ($dom->Divided, 'Merge_Divided()');

$dom->Divide;
$dom->L->Type ('O');
$dom->R->Type ('O');

ok ($dom->Divided, 'Merge_Divided()');
$dom->Merge_Divided;
ok (!$dom->Divided, 'Merge_Divided()');

use Urb::Dom::Mutate;
ok (!$dom->Above, 'no level above');
Urb::Dom::Mutate::level_swap ($dom);
ok ($dom->Above, 'level above after level_swap()');

$dom->Above->Type ('C');
is ($dom->Above->Type, 'C', 'set type');
Urb::Dom::Mutate::level_swap ($dom);
is ($dom->Above->Type, 'S', 'level_swap()');

Urb::Dom::Mutate::level_swap ($dom->Above);
is ($dom->Above->Type, 'C', 'level_swap()');
is ($dom->Type, 'S', 'level_swap()');

1;
