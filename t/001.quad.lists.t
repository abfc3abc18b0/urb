#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that return lists of Urb::Quad objetcs

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

is ((join ':', map {sort $_->Id} $quad->Leafs), 'll:lrl:lrr:rl:rrl:rrr', 'leaf ids');
is ((join ':', map {sort $_->Id} $quad->Left->Leafs), 'll:lrl:lrr', 'leaf ids');
is ((join ':', map {sort $_->Id} $quad->Right->Leafs), 'rl:rrl:rrr', 'leaf ids');

is ((join ':', map {sort $_->Id} $quad->Branches), ':l:lr:r:rr', 'branch ids');
is ((join ':', map {sort $_->Id} $quad->Left->Branches), 'l:lr', 'branch ids');
is ((join ':', map {sort $_->Id} $quad->Right->Branches), 'r:rr', 'branch ids');

is ((join ':', map {$_->Id} $quad->Right->Right->Left->Parents), 'rr:r:', 'parents');
is ((join ':', map {$_->Id} $quad->Right->Right->Right->Parents), 'rr:r:', 'parents');
is ((join ':', map {$_->Id} $quad->Left->Right->Right->Parents), 'lr:l:', 'parents');

is ((join ':', map {$_->Id} $quad->Left->Right->Right->Children), 'lrr', 'children');
is ((join ':', map {$_->Id} $quad->Left->Children), 'l:ll:lr:lrl:lrr', 'children');
is ((join ':', map {$_->Id} $quad->Children), ':l:ll:lr:lrl:lrr:r:rl:rr:rrl:rrr', 'children');

1;
