#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that query Urb::Quad Ids

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

is ((join ',', @{$quad->Min}), '1,1', 'min is 1,1');
is ((join ',', @{$quad->Max}), '4,6', 'max is 4,6');

like ($quad->Orientation->[0], '/^-0.857/', 'orientation');
like ($quad->Orientation->[1], '/^-0.514/', 'orientation');

like ($quad->Orientation_Perpendicular->[0], '/^0.514/', 'orientation perpendicular');
like ($quad->Orientation_Perpendicular->[1], '/^-0.857/', 'orientation perpendicular');

$quad->{node}->[3]->[0] = -1;
$quad->{node}->[1]->[0] = 5;
$quad->{node}->[1]->[1] = -1;

is ((join ',', @{$quad->Min}), '-1,-1', 'min is -1,-1');
is ((join ',', @{$quad->Max}), '5,6', 'max is 5,6');

1;
