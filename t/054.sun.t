#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Field::Occlusion;
use Urb::Misc::Sun;

my $dom = Urb::Dom->new;

# a square single room house
$dom->{node} = [[0.0,0.0],[16.0,0.0],[16.0,16.0],[0.0,16.0]];
$dom->{perimeter} = {a => 'private', b => undef, c => undef, d => undef};
$dom->Type ('L');

my @walls = $dom->Walls;
is (scalar (@walls), 4, 'no walls');

my $field = Urb::Field::Occlusion->new;
$field->{_walls} = \@walls;
$field->{nocache} = 1;
$field->{origin} = [-5,-5,0];

# Sheffield latitude
my $sun = Urb::Misc::Sun->new (53.0);

# four points outside each wall of the house
my $north = $field->Sun_horizontal ($sun, [8.0, 18.0, 1.0]);
my $south = $field->Sun_horizontal ($sun, [8.0, -2.0, 1.0]);
my $east = $field->Sun_horizontal ($sun, [18.0, 8.0, 1.0]);
my $west = $field->Sun_horizontal ($sun, [-2.0, 8.0, 1.0]);
#use Data::Dumper; die Dumper $field;

#use Data::Dumper; die Dumper $north, $south, $east, $west;

ok ($north < $south, 'south-side is sunnier than north-side');
ok ($north < $east, 'east is sunnier than north');
ok ($north < $west, 'west is sunnier than north');
ok ($south > $east, 'south is sunnier than east');
ok ($south > $west, 'south is sunnier than west');

my $north_up = $field->Sun_horizontal ($sun, [8.0, 18.0, 2.0]);
ok ($north_up > $north, 'north-side is sunnier higher up');

my $north_up_up = $field->Sun_horizontal ($sun, [8.0, 18.0, 3.0]);
ok ($north_up_up > $north_up, 'north-side is even sunnier at eaves level');
ok ($north_up_up > $south, 'north-side at eaves level is sunnier than south side');

#is ($east, $west, 'east and west side are equally sunny');

my $south_up = $field->Sun_horizontal ($sun, [8.0, -2.0, 2.0]);
ok ($south_up > $south, 'south-side is sunnier higher up');
ok ($south_up > $north_up, 'south-side is sunnier than north-side');

my $south_up_up = $field->Sun_horizontal ($sun, [8.0, -2.0, 3.0]);
is ($south_up_up, $north_up_up, 'south-side at eaves level is as sunny as north-side at eaves level');

my $north_east = $field->Sun_horizontal ($sun, [12.0, 18.0, 1.0]);
my $north_west = $field->Sun_horizontal ($sun, [4.0, 18.0, 1.0]);

ok ($north_east > $north, 'relative');
ok ($north_west > $north, 'relative');

my $west_north = $field->Sun_horizontal ($sun, [-2.0, 12.0, 1.0]);
my $west_south = $field->Sun_horizontal ($sun, [-2.0, 4.0, 1.0]);

ok ($west_north < $west, 'relative');
ok ($west_south > $west, 'relative');

my $east_north = $field->Sun_horizontal ($sun, [18.0, 12.0, 1.0]);
my $east_south = $field->Sun_horizontal ($sun, [18.0, 4.0, 1.0]);

ok ($east_north < $east, 'relative');
ok ($east_south > $east, 'relative');

# FIXME $east should be =~ $west
# FIXME $west_north should be =~ $east_north
#die join ' ', $north_east, $north_west;

1;
