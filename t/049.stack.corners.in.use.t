#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Dom') };

my $dom = Urb::Dom->new;

$dom->{node} = [[-1,1],[9,-1],[11,9],[1,11]];

$dom->Divide;
$dom->L->Divide;
$dom->L->Rotate;
$dom->L->L->Type ('C');
$dom->L->R->Type ('B');
$dom->R->Type ('L');

# 3               2
#  +------+------+
#  |2    1|3    2|
#  |  LR  |      |
#  |3    0|      |
#  +------+   R  |
#  |2    1|      |
#  |  LL  |      |
#  |3    0|0    1|
#  +------+------+
# 0               1

$dom->Clone_Above;
$dom->Clone_Above;

my @graph = ($dom->Graph, $dom->Above->Graph, $dom->Above->Above->Graph);

my @corners_in_use = $dom->L->L->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 1, 'one corner in use');
is ($corners_in_use[0], 1, 'corner is 1');

@corners_in_use = $dom->L->R->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 0, 'no corner in use');
@corners_in_use = $dom->R->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 0, 'no corner in use');

$dom->L->L->Rotate;
@corners_in_use = $dom->L->L->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 1, 'one corner in use');
is ($corners_in_use[0], 0, 'corner is 0');

$dom->L->L->Rotate;
@corners_in_use = $dom->L->L->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 1, 'one corner in use');
is ($corners_in_use[0], 3, 'corner is 3');

$dom->L->L->Above->Rotate;
$dom->L->L->Above->Above->Unrotate;
@corners_in_use = $dom->L->L->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 1, 'one corner in use');
is ($corners_in_use[0], 3, 'corner is 3');

my $riser = 0.21;
my $stair_width = 1.25;

ok ($dom->L->L->Stair_Fit ($riser, $stair_width, @corners_in_use) > 1, 'can fit stair');

$dom->Above->R->Divide (0.75,0.75);
$dom->R->Unrotate;

# 3               2
#  +------+------+
#  |2    1|0    3|
#  |  LR  |      |
#  |3    0|  RL  |
#  +------+      |
#  |0    3|1    2|
#  |  LL  +------+
#  |      |0 RR 3|
#  |1    2|1    2|
#  +------+------+
# 0               1

@graph = ($dom->Graph, $dom->Above->Graph, $dom->Above->Above->Graph);

@corners_in_use = $dom->L->L->Stack_Corners_In_Use (@graph);
is (scalar @corners_in_use, 2, 'two corners in use');
is ($corners_in_use[0], 2, 'corner is 2');
is ($corners_in_use[1], 3, 'corner is 3');

ok ($dom->L->L->Stair_Fit ($riser, $stair_width, @corners_in_use) > 1, 'can fit stair');

# FIXME this nearly succeeds, but shouldn't be anywhere near
ok ($dom->L->L->Stair_Fit ($riser, $stair_width, 0, 1, @corners_in_use) < 1, 'can\'t fix stair');

1;
