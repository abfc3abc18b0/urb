#!/usr/bin/perl

#Editor vim:syn=perl

use strict;
use warnings;
use Test::More 'no_plan';

use lib ('lib', '../lib');
use_ok ('Urb::Polygon');

my $polygon = Urb::Polygon->new ([0,0], [4,0], [4,3], [0,0]);

is ($polygon->area, 6, 'area');
is ($polygon->perimeter, 3+4+5, 'perimeter');
is ($polygon->nrPoints, 4, 'four points');
ok ($polygon->isClosed, 'closed');
ok (!$polygon->isClockwise, 'anti-clockwise');

1;
