#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Dom') };

my $dom = Urb::Dom->new;

$dom->Deserialise (YAML::LoadFile ('t/data/dom.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

for my $leaf ($dom->Leafs)
{
    ok ($leaf->Perpendicular (0.3) > 0.6, 'all 8 deg from perpendicular not so good');
}

ok ($dom->By_Id ('ll')->Proportion (1.5, 0.5) < 0.03, '3:1 bad aspect ratio');
ok ($dom->By_Id ('rl')->Proportion (1.5, 0.5) < 0.03, '7:2 bad aspect ratio');
ok ($dom->By_Id ('rrr')->Proportion (1.5, 0.5) < 0.03, '7:2 bad aspect ratio');

ok ($dom->By_Id ('lrl')->Proportion (1.5, 0.5) > 0.9, '6:5 good aspect ratio');
ok ($dom->By_Id ('rrl')->Proportion (1.5, 0.5) > 0.9, '8:5 good aspect ratio');

like ($dom->By_Id ('lrr')->Proportion (1.5, 0.5), '/^0.6/' , '2:1 ok aspect ratio');

ok ($dom->By_Id ('ll')->Size  (7.0, 2.7) < 0.05, '17.5 bad');
ok ($dom->By_Id ('lrl')->Size (7.0, 2.7) > 0.8, '5.25 ok');
ok ($dom->By_Id ('lrr')->Size (7.0, 2.7) < 0.2, '12.25 bad');
ok ($dom->By_Id ('rl')->Size  (7.0, 2.7) > 0.99, '7 perfect');
ok ($dom->By_Id ('rrl')->Size (7.0, 2.7) < 0.05, '19.6 bad');
ok ($dom->By_Id ('rrr')->Size (7.0, 2.7) > 0.8, '8.4 ok');

ok ($dom->By_Id ('ll')->Width  (2.5, 0.1) > 0.9, '2.5 good');
ok ($dom->By_Id ('lrl')->Width (2.5, 0.1) < 0.1, '2,1 bad');
ok ($dom->By_Id ('lrr')->Width (2.5, 0.1) > 0.9, '2.5 good');
ok ($dom->By_Id ('rl')->Width  (2.5, 0.1) < 0.1, '1.4 bad');
ok ($dom->By_Id ('rrl')->Width (2.5, 0.1) > 0.9, '3.5 good');
ok ($dom->By_Id ('rrr')->Width (2.5, 0.1) < 0.1, '1.5 bad');

