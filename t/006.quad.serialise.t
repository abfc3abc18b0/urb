#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that serialise Urb::Quad

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

my @list = $quad->Serialise;

my $quad_b = Urb::Quad->new;
$quad_b->Deserialise (@list);

$quad->Clean_Cache;
is_deeply ($quad, $quad_b, 'deserialised serialised quad is identical') if $Test::More::VERSION >= 0.49;

my $yaml = YAML::Dump ($quad->Serialise);
my $quad_c = Urb::Quad->new;
$quad_c->Deserialise (YAML::Load ($yaml));

$quad->Clean_Cache;
is_deeply ($quad, $quad_c, 'deserialised unYAMLed YAMLed serialised quad is identical') if $Test::More::VERSION >= 0.49;

1;
